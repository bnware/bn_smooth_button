library app_init;

import 'package:angular2/platform/browser.dart';

import 'app_component.dart';
import 'package:logging/logging.dart';
import 'package:logging_handlers/browser_logging_handlers.dart';

void main () {
  Logger.root.level = Level.FINEST;
  Logger.root.onRecord.listen(new LogPrintHandler());

  bootstrap(AppComponent);
}