library app_component;

import 'package:angular2/angular2.dart';
import 'package:bn_smooth_button/bn_smooth_button.dart';

@Component(
    selector: "app-component",
    templateUrl: "app_component.html",
    directives: const [BnSmoothButton]
)
class AppComponent {}