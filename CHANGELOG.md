# Changelog

## 2.0.0
- updated to Angular 2 
- changed it from being a component to a directive 
  (helps better with semantics of the html, like applying bn-smooth-box to 
   header, footer, section, etc.)

## 1.0.0

- Initial version
