library bn_smooth_button.component;

import 'package:angular2/angular2.dart';
import 'dart:html';

@Component(
    selector: "bn-smooth-button",
    templateUrl: "template.html"
)
class BnSmoothButton implements OnInit {

  @ViewChild("button")
  ElementRef button;

  bool _disabled = false;

  @Input()
  set disabled(String value) {
    _disabled = (value is String) ? true : false;
    applyToButton();
  }

  void applyToButton() {
    button?.nativeElement?.disabled = _disabled;
  }

  BnSmoothButton(ElementRef node) {
    node.nativeElement.style.display = "inline-block";
  }


  @override
  ngOnInit() {
    applyToButton();
  }
}